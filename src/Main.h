// define header
#ifndef MAIN_H
#define MAIN_H

// import C++ libs
#include <stdio.h>
#include <SDL2/SDL.h>
#include <list>
#include <iterator>
#include <random>
#include <string>
#include <chrono>

// import project files
#include "Block.h"
#include "Player.h"
#include "Draw.h"

// needed for Windows computers
#define SDL_main main

// modify namespace
using namespace std::chrono;

// define settings
int SCREEN_HEIGHT = 700; // how many pixels tall the window should be
int SCREEN_WIDTH = 700; // how many pixels wide the screen should be
double PLAYER_SPEED = (double) 1/3; // how fast the player should move (in world units per second)
bool DEBUG_KEYS = false; // should special keybinds be active for debugging the game (true=yes, false=no)
double BLOCK_SPAWN_DELAY = 3; // how many seconds should it take to spawn another block
double BLOCK_SPEED = (double) 1/10; //  how fast the blocks move (in world units per second)
double BLOCK_OFFSET = (double) 5/1000; // the offset of the red, green, and blue blocks (in world space)
double BLOCK_WIDTH = (double) 2/10; // the width of generated blocks (in world space)
double BLOCK_HEIGHT = (double) 1/20; // the height of generated blocks (in world space)
double JUMP_TIME_MAX = 0.33; // how long it takes to complete a jump (in seconds)
double JUMP_BOOST = 15; // the boost given to the player's jump calculation (in world space)
double GRAVITY = 1; // how fast the player is pulled down (in world units per second)

// declare resources
nanoseconds CLOCK_OLD; // the timestamp of the last frame
nanoseconds CLOCK_NEW; // the timestamp of the current frame
double CLOCK_DELTA; // the amount of time it took to render a frame
double BLOCK_TIMER; // how many seconds until the next block spawns

/**
 * This controls what the game should be doing at any moment in time
 * 
 * There are four valid states:
 *      0 = waiting: the game just launched and is waiting until the player is ready
 *      1 = playing: the game is being played
 *      2 = dead: the player died and is waiting on the player to exit
 *      3 = exit: the game should close itself
 *      4 = yield: the game is stopped until the player resumes it
 * 
 * NOTE: an invalid state will result in the game switching to state 3 (exit)
 */
int GAME_STATE;

// declare functions
void init();
void stop(SDL_Window, SDL_Renderer);
nanoseconds getTime();
void calcDeltaTime();
int main();

// end header
#endif