// define header
#ifndef PLAYER_H
#define PLAYER_H

// inform compiler of needed libs
#include <SDL2/SDL.h>
#include <list>
#include <string>
#include <random>

// inform compiler of needed files
#include "Block.h"

// inform compiler of needed globals (defined in main.cpp)
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern double CLOCK_DELTA;
extern double PLAYER_SPEED;
extern double BLOCK_SPEED;
extern double JUMP_TIME_MAX;
extern double JUMP_BOOST;
extern double GRAVITY;

// declare class for player
class Player {
    public:
        double x;
        double y;
        double width;
        double height;
        double scale;
        double velocity;
        double jumpTime;
        int jumpLimit;
        int jumpCount;
        int score;
        double glitchOffset;
        bool isGlitched;
        bool isAlive;
        bool showHitbox;
        std::list <Block> *blocks;
        SDL_Renderer *sdlRenderer;
        SDL_Rect player_antenna;
        SDL_Rect player_head;
        SDL_Rect player_eye;
        SDL_Rect player_neck;
        SDL_Rect player_torso;
        SDL_Rect player_right_leg;
        SDL_Rect player_right_foot;
        Player();
        Player(std::list <Block> *, SDL_Renderer *);
        Player(double, double, double, bool, double,std::list <Block> *, SDL_Renderer *);
        std::string status();
        void transformX(double delta);
        void transformY(double delta);
        void checkDeath();
        double getRenderOffset();
        void render();
        void updateScore(int delta);
        void runPhysics();
        void toggleGlitched();
        void toggleHitbox();
        void update();
        void jump();
};

// end header
#endif