// import block class
#include "Block.h"

/**
 * This is the no-arg constructor for the block class
 * 
 * NOTE: This is not implemented and only exists to prevent compiler errors
 */
Block::Block() {}

/**
 * This constructs a block object
 * 
 * @param _x The world X coordinate of the block
 * @param _y The world Y coordinate of the block
 * @param _width The width of the block in world units
 * @param _height The height of the block in world units
 * @param _offset The intensity of the glitch effect
 * @param _rend The SDL_Renderer to draw the player with
 */
Block::Block(double _x, double _y, double _width, double _height, double _offset, SDL_Renderer *_rend) {
    x = _x;
    y = _y;
    width = _width;
    height = _height;
    offset = _offset;
    rend = _rend;
    active = true;
    scoreClaimed = false;
    showHitbox = false;
    redRect = SDL_Rect();
    greenRect = SDL_Rect();
    blueRect = SDL_Rect();
}

/**
 * This is a helper method that calculates an offset position for the block to be drawn
 * 
 * @return an integer with an offset position to draw the block
 */
double Block::__calcOffset__(bool axis) {
    if (axis) {
        // calculate for y
        if (active) {
            // NOTE: '(pow(-1, (rand()%2)+1)*random())' returns either 1 or -1
            // NOTE: '((pow(-1, (rand()%2)+1)*random()) / (float) RAND_MAX)' forces the number to be a float between 0 and 1
            // NOTE: ' * offset)' scales the random number by the offset value (0-1 -> 0-offset)
            // NOTE: 'y + ' applies the normal position to the offset value
            return y + (((pow(-1, (rand()%2)+1)*rand()) / (float) RAND_MAX) * offset);
        } else {
            return y;
        }
    } else {
        // calculate for x
        if (active) {
            // NOTE: '(pow(-1, (rand()%2)+1)*random())' returns either 1 or -1
            // NOTE: '((pow(-1, (rand()%2)+1)*random()) / (float) RAND_MAX)' forces the number to be a float between 0 and 1
            // NOTE: '* offset)' scales the random number by the offset value (0-1 -> 0-offset)
            // NOTE: 'x + ' applies the normal position to the offset value
            return x + (((pow(-1, (rand()%2)+1)*rand()) / (float) RAND_MAX) * offset);
        } else {
            return x;
        }
    }
}

/**
 * This corrects the end coordinate position of a block so it isn't rendered outside the screen
 * 
 * @param startValue the x or y position of the block
 * @param axis sets either the x or y axis to be calculated (true for y)
 * @return the new end position of the block
 */
double Block::__calcBounds__(double startValue, bool axis) {
    if (axis) {
        // calculate for y
        double target = startValue + height;
        if (target < 0) {
            return 0;
        } else if (target > 1) {
            return 1;
        } else {
            return target;
        }
    } else {
        // calculate for x
        double target = startValue + width;
        if (target < 0) {
            return 0;
        } else if (target > 1) {
            return 1;
        } else {
            return target;
        }
    }
}

/**
 * This function returns a status string with debug information about the block.
 * 
 * Things included:
 *      - position on screen
 *      - width/height
 *      - if the glitch effect is active
 *      - the intensity of the glitch effect
 *      - if the block score is claimed
 * 
 * @return string containing debug information
 */
std::string Block::status(){
    return "(X/Y): ("
        + std::to_string(x)
        + ","
        + std::to_string(y)
        + ")\n(w/h): ("
        + std::to_string(width)
        + ","
        + std::to_string(height)
        + ")\nglitched: "
        + std::to_string(active)
        + "\nglitchOffset: "
        + std::to_string(offset)
        + "\nisClaimed: "
        + std::to_string(scoreClaimed);
}

/**
 * This renders the block to the screen
 */
void Block::render() {
    // PHASE 1: Calculate block positions
    double redStartX = __calcOffset__(X_AXIS);
    double redStopX = __calcBounds__(redStartX, X_AXIS);
    double redStartY = __calcOffset__(Y_AXIS);
    double redStopY = __calcBounds__(redStartY, Y_AXIS);
    double greenStartX = __calcOffset__(X_AXIS);
    double greenStopX = __calcBounds__(greenStartX, X_AXIS);
    double greenStartY = __calcOffset__(Y_AXIS);
    double greenStopY = __calcBounds__(greenStartY, Y_AXIS);
    double blueStartX = __calcOffset__(X_AXIS);
    double blueStopX = __calcBounds__(blueStartX, X_AXIS);
    double blueStartY = __calcOffset__(Y_AXIS);
    double blueStopY = __calcBounds__(blueStartY, Y_AXIS);
    // PHASE 2: Translate world space to screen space
    redStartX *= SCREEN_WIDTH;
    redStopX *= SCREEN_WIDTH;
    redStartY *= SCREEN_HEIGHT;
    redStopY *= SCREEN_HEIGHT;
    greenStartX *= SCREEN_WIDTH;
    greenStopX *= SCREEN_WIDTH;
    greenStartY *= SCREEN_HEIGHT;
    greenStopY *= SCREEN_HEIGHT;
    blueStartX *= SCREEN_WIDTH;
    blueStopX *= SCREEN_WIDTH;
    blueStartY *= SCREEN_HEIGHT;
    blueStopY *= SCREEN_HEIGHT;
    // PHASE 3: Store positions in rectangles
    this->redRect.x = redStartX;
    this->redRect.y = redStartY;
    this->redRect.w = (redStopX - redStartX);
    this->redRect.h = (redStopY - redStartY);
    this->greenRect.x = greenStartX;
    this->greenRect.y = greenStartY;
    this->greenRect.w = (greenStopX - greenStartX);
    this->greenRect.h = (greenStopY - greenStartY);
    this->blueRect.x = blueStartX;
    this->blueRect.y = blueStartY;
    this->blueRect.w = (blueStopX - blueStartX);
    this->blueRect.h = (blueStopY - blueStartY);
    // PHASE 4: Draw rectangles
    // prepare renderer
    SDL_SetRenderDrawBlendMode(rend, SDL_BLENDMODE_ADD);
    // add red value
    SDL_SetRenderDrawColor(rend, 255, 0, 0, 255);
    SDL_RenderFillRect(rend, &redRect);
    // add green value
    SDL_SetRenderDrawColor(rend, 0, 255, 0, 255);
    SDL_RenderFillRect(rend, &greenRect);
    // add blue value
    SDL_SetRenderDrawColor(rend, 0, 0, 255, 255);
    SDL_RenderFillRect(rend, &blueRect);
    // update renderer
    SDL_SetRenderDrawBlendMode(rend, SDL_BLENDMODE_NONE);
    //make hitbox
    if (showHitbox) {
        // PHASE 1: Calculate values
        double line1X1 = x;
        double line1Y1 = y;
        double line1X2 = x+width;
        double line1Y2 = y;
        double line2X1 = x;
        double line2Y1 = y+height;
        double line2X2 = x+width;
        double line2Y2 = y+height;
        double line3X1 = x;
        double line3Y1 = y;
        double line3X2 = x;
        double line3Y2 = y+height;
        double line4X1 = x+width;
        double line4Y1 = y;
        double line4X2 = x+width;
        double line4Y2 = y+height;
        // PHASE 2: Translate world space to screen space
        line1X1 *= SCREEN_WIDTH;
        line1Y1 *= SCREEN_HEIGHT;
        line1X2 *= SCREEN_WIDTH;
        line1Y2 *= SCREEN_HEIGHT;
        line2X1 *= SCREEN_WIDTH;
        line2Y1 *= SCREEN_HEIGHT;
        line2X2 *= SCREEN_WIDTH;
        line2Y2 *= SCREEN_HEIGHT;
        line3X1 *= SCREEN_WIDTH;
        line3Y1 *= SCREEN_HEIGHT;
        line3X2 *= SCREEN_WIDTH;
        line3Y2 *= SCREEN_HEIGHT;
        line4X1 *= SCREEN_WIDTH;
        line4Y1 *= SCREEN_HEIGHT;
        line4X2 *= SCREEN_WIDTH;
        line4Y2 *= SCREEN_HEIGHT;
        // PHASE 3: Render lines
        SDL_SetRenderDrawColor(rend, 255, 105, 180, 255);
        SDL_RenderDrawLine(rend, line1X1, line1Y1, line1X2, line1Y2);
        SDL_RenderDrawLine(rend, line2X1, line2Y1, line2X2, line2Y2);
        SDL_RenderDrawLine(rend, line3X1, line3Y1, line3X2, line3Y2);
        SDL_RenderDrawLine(rend, line4X1, line4Y1, line4X2, line4Y2);
    }
    //restore render color
    SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
}

/**
 * This translates the block on the X axis
 * 
 * @param delta the amount of pixels to move the block by
 */
void Block::transformX(double delta) {
    double newX = x + delta;
    // NOTE: used -0.1 for min x so blocks can move far enough to be deleted
    if (newX+width >= -0.1 && newX-width <= 1) {
        x = newX;
    }
}

/**
 * This translates the block on the Y axis
 * 
 * @param delta the amount of pixels to move the block by
 */
void Block::transformY(double delta) {
    double newY = y + delta;
    if (newY+height >= 0 && newY-height <= 1) {
        y = newY;
    }
}

/**
 * This modifies the width of the block
 * 
 * @param delta the number of pixels to add to the width
 */
void Block::changeWidth(double delta) {
    double newWidth = width + delta;
    if (newWidth > 0) {
        width = newWidth;
    }
}

/**
 * This modifies the height of the block
 * 
 * @param delta the number of pixels to add to the height
 */
void Block::changeHeight(double delta) {
    double newHeight = height + delta;
    if (newHeight > 0) {
        height = newHeight;
    }
}

/**
 * This modifies the intensity of the glitch effect
 * 
 * @param delta the offset in pixels to be added to the current glitch offset
 */
void Block::changeOffset(double delta) {
    double newOffset = offset + delta;
    if (newOffset >= 0) {
        offset = newOffset;
    }
}

/**
 * This toggles the glitch effect for the player
 */
void Block::toggleActive() {
    active = !active;
}

/**
 * This toggles if the block's hitbox should be seen
 */
void Block::toggleHitbox() {
    showHitbox = !showHitbox;
}

/**
 * This attempts to claim the score from a block
 * 
 * @return true if points were claimed successfully
 */
bool Block::claimScore() {
    if (!scoreClaimed) {
        scoreClaimed = true;
        return true;
    }
    else {
        return false;
    }
}

/**
 * Checks if a block moved outside the left side of the screen
 * 
 * @return true if block is outside the screen
 */
bool Block::shouldDie() {
    return (x+width < 0);
}

/**
 * This spawns a block on the right side of the screen at a random height
 * 
 * @param blocks the list of blocks to add the new block
 * @param rend the SDL_Renderer that draws the block to the screen
 * @param screen the SDL_Surface to draw the block onto
 */
void spawnBlock(std::list <Block> *blocks, SDL_Renderer *rend) {
    double blockXPosition = 1;
    //                          Value between 0 and 1           Scale spawn area      Center spawn area
    double blockYPosition = (((double) rand() / RAND_MAX) * (1 - (BLOCK_HEIGHT * 2))) + BLOCK_HEIGHT;
    //                       x               y               w            h             o             r
    blocks->push_front(Block(blockXPosition, blockYPosition, BLOCK_WIDTH, BLOCK_HEIGHT, BLOCK_OFFSET, rend));
}

/**
 * This processes all blocks for a single game tick
 * 
 * Things processed:
 *      - block movement
 *      - removing old blocks
 *      - spawning new blocks
 * 
 * @param blocks list of blocks to process
 * @param rend the SDL_Renderer used to draw blocks
 */
void updateBlocks(std::list <Block> *blocks, SDL_Renderer *rend) {
    // update block positions
    for (std::list<Block>::iterator i = blocks->begin(); i != blocks->end(); std::advance(i,1)) {
        i->transformX(-1*BLOCK_SPEED*CLOCK_DELTA);
    }
    // check if blocks need to die
    for (std::list<Block>::iterator i = blocks->begin(); i != blocks->end(); std::advance(i,1)) {
        if (i->shouldDie()) {
            i = blocks->erase(i);
        }
    }
    // check if new block needs to spawn
    if (BLOCK_TIMER > 0) {
        // do not spawn block
        BLOCK_TIMER -= CLOCK_DELTA;
    } else {
        // spawn block
        BLOCK_TIMER = BLOCK_SPAWN_DELAY;
        spawnBlock(blocks, rend);
    }
}