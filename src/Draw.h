// define header
#ifndef DRAW_H
#define DRAW_H

// inform compiler of needed libs
#include <SDL2/SDL.h>
#include <list>
#include <random>

// inform compiler of needed files
#include "Player.h"

// inform compiler of needed globals (defined in main.cpp)
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern double CLOCK_DELTA;

// declare all functions inside Draw.cpp
void deathScreen(SDL_Renderer *rend);
void drawNumber(SDL_Renderer *rend, int number, int startX, int stopX, int startY, int stopY);
void drawScore(SDL_Renderer *rend, int score);
void pauseScreen(SDL_Renderer *rend);
void renderGameScene(std::list <Block> blocks, SDL_Renderer *rend, Player *player);
void renderYieldScene(std::list <Block> blocks, SDL_Renderer *rend, Player *player);

// end header
#endif