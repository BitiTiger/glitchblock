// import player class
#include "Player.h"

/**
 * This is the no-arg constructor for the player
 * 
 * NOTE: This is not implemented and only exists to prevent compiler errors
 */
Player::Player() {}

/**
 * This constructs a player with default settings.
 * 
 * @param _sdlRenderer The SDL_Renderer to draw the player with
 * @param _blocks The list of block objects the player can interact with
 */
Player::Player(std::list <Block> *_blocks, SDL_Renderer *_sdlRenderer) {
    x = 0.5;
    y = 0;
    scale = 4;
    width = scale*15;
    height = scale*26;
    score = 0;
    showHitbox = false;
    isAlive = true;
    glitchOffset = 5;
    isGlitched = true;
    velocity = 0.0;
    jumpTime = 0;
    jumpLimit = 2;
    jumpCount = 0;
    blocks = _blocks;
    sdlRenderer = _sdlRenderer;
    player_antenna = SDL_Rect();
    player_head = SDL_Rect();
    player_eye = SDL_Rect();
    player_neck = SDL_Rect();
    player_torso = SDL_Rect();
    player_right_leg = SDL_Rect();
    player_right_foot = SDL_Rect();
}

/**
 * This constructs a player with custom settings.
 * 
 * @param _x The initial X position of the player in screen coordinates
 * @param _y The initial Y position of the player in screen coordinates
 * @param _scale Defines how wide/tall the player should be
 * @param _isGlitched Determines if the glitch effect is used
 * @param _glitchOffset The intensity of the glitch effect
 * @param _blocks The list of block objects the player can interact with
 * @param _sdlRenderer The SDL_Renderer to draw the player with
 */
Player::Player(double _x, double _y, double _scale, bool _isGlitched, double _glitchOffset, std::list <Block> *_blocks, SDL_Renderer *_sdlRenderer) {
    x = _x;
    y = _y;
    scale = _scale;
    width = scale*16;
    height = scale*26;
    score = 0;
    isAlive = true;
    blocks = _blocks;
    glitchOffset = _glitchOffset;
    isGlitched = _isGlitched;
    showHitbox = false;
    velocity = 0.0;
    jumpTime = 0;
    jumpLimit = 2;
    jumpCount = 0;
    sdlRenderer = _sdlRenderer;
    player_antenna = SDL_Rect();
    player_head = SDL_Rect();
    player_eye = SDL_Rect();
    player_neck = SDL_Rect();
    player_torso = SDL_Rect();
    player_right_leg = SDL_Rect();
    player_right_foot = SDL_Rect();
}

/**
 * This function returns a status string with debug information about the player.
 * 
 * Things included:
 *      - position on screen
 *      - width/height
 *      - if the glitch effect is active
 *      - the intensity of the glitch effect
 *      - if the player is alive
 * 
 * @return string containing debug information
 */
std::string Player::status(){
    return "(X/Y): ("
        + std::to_string(x)
        + ","
        + std::to_string(y)
        + ")\n(w/h): ("
        + std::to_string(width)
        + ","
        + std::to_string(height)
        + ")\nglitched: "
        + std::to_string(isGlitched)
        + "\nglitchOffset: "
        + std::to_string(glitchOffset)
        + "\nisAlive: "
        + std::to_string(isAlive)
        + "\nshowHitbox: "
        + std::to_string(showHitbox);
}

/**
 * This translates the player on the X axis
 * 
 * @param delta the amount of pixels to move the player by
 */
void Player::transformX(double delta) {
    if (isAlive) {
        // NOTE: allow the player to go off screen and die
        x += delta;
    }
}

/**
 * This translates the player on the Y axis
 * 
 * @param delta the amount of pixels to move the player by
 */
void Player::transformY(double delta) {
    if (isAlive) {
        // NOTE: allow the player to go off screen and die
        y += delta;
    }
}

/**
 * This checks if the player should be dead
 * 
 * NOTE: the player is dead if they are outside the screen (except for the top side)
 */
void Player::checkDeath() {
    if (isAlive) {
        isAlive = (y<=1 && x<=1 && x+width>=0);
    }
}

/**
 * This is a helper method that calculates an offset position for the player to be drawn
 * 
 * @return an integer with an offset position to draw the player
 */
double Player::getRenderOffset() {
    if (isGlitched) {
        return (((pow(-1, (rand()%2)+1)*rand()) / (float) RAND_MAX) * (glitchOffset));
    } else {
        return 0;
    }
}

/**
 * This renders the player to the screen
 * 
 * NOTE: I swear this is some black magic voodoo type stuff, don't edit this code unless you like self-harm
 */
void Player::render() {
    //configure renderer
    SDL_SetRenderDrawBlendMode(sdlRenderer, SDL_BLENDMODE_ADD);
    // make vars to recycle
    double useX;
    double useY;
    double centerX;
    // render RGB values
    for (int i=0; i<3; i++) {
        // PHASE 1: get some render settings
        useX = x + getRenderOffset();
        useY = y + getRenderOffset();
        centerX = useX+(scale*7);
        // PHASE 2: calculate rectangle values
        double playerAntennaX = centerX - (scale*5);
        double playerAntennaY = useY;
        double playerAntennaW = scale*1;
        double playerAntennaH = scale*2;
        double playerHeadX = centerX - (scale*7);
        double playerHeadY = useY + scale*2;
        double playerHeadW = scale*14;
        double playerHeadH = scale*5;
        double playerEyeX = centerX + (scale*7);
        double playerEyeY = useY + scale*3;
        double playerEyeW = scale*1;
        double playerEyeH = scale*1;
        double playerNeckX = centerX - (scale*4);
        double playerNeckY = useY + scale*7;
        double playerNeckW = scale*8;
        double playerNeckH = scale*1;
        double playerTorsoX = centerX - (scale*5);
        double playerTorsoY = useY + scale*8;
        double playerTorsoW = scale*10;
        double playerTorsoH = scale*10;
        double playerRightLegX = centerX - (scale*3);
        double playerRightLegY = useY + scale*18;
        double playerRightLegW = scale*2;
        double playerRightLegH = scale*6;
        double playerRightFootX = centerX - (scale*3);
        double playerRightFootY = useY + scale*24;
        double playerRightFootW = scale*7;
        double playerRightFootH = scale*2;
        // PHASE 3: translate world space to screen space
        playerAntennaX *= SCREEN_WIDTH;
        playerAntennaY *= SCREEN_HEIGHT;
        playerAntennaW *= SCREEN_WIDTH;
        playerAntennaH *= SCREEN_HEIGHT;
        playerHeadX *= SCREEN_WIDTH;
        playerHeadY *= SCREEN_HEIGHT;
        playerHeadW *= SCREEN_WIDTH;
        playerHeadH *= SCREEN_HEIGHT;
        playerEyeX *= SCREEN_WIDTH;
        playerEyeY *= SCREEN_HEIGHT;
        playerEyeW *= SCREEN_WIDTH;
        playerEyeH *= SCREEN_HEIGHT;
        playerNeckX *= SCREEN_WIDTH;
        playerNeckY *= SCREEN_HEIGHT;
        playerNeckW *= SCREEN_WIDTH;
        playerNeckH *= SCREEN_HEIGHT;
        playerTorsoX *= SCREEN_WIDTH;
        playerTorsoY *= SCREEN_HEIGHT;
        playerTorsoW *= SCREEN_WIDTH;
        playerTorsoH *= SCREEN_HEIGHT;
        playerRightLegX *= SCREEN_WIDTH;
        playerRightLegY *= SCREEN_HEIGHT;
        playerRightLegW *= SCREEN_WIDTH;
        playerRightLegH *= SCREEN_HEIGHT;
        playerRightFootX *= SCREEN_WIDTH;
        playerRightFootY *= SCREEN_HEIGHT;
        playerRightFootW *= SCREEN_WIDTH;
        playerRightFootH *= SCREEN_HEIGHT;
        // PHASE 4: create rectangles
        player_antenna.x = playerAntennaX;
        player_antenna.y = playerAntennaY;
        player_antenna.w = playerAntennaW;
        player_antenna.h = playerAntennaH;
        player_head.x = playerHeadX;
        player_head.y = playerHeadY;
        player_head.w = playerHeadW;
        player_head.h = playerHeadH;
        player_eye.x = playerEyeX;
        player_eye.y = playerEyeY;
        player_eye.w = playerEyeW;
        player_eye.h = playerEyeH;
        player_neck.x = playerNeckX;
        player_neck.y = playerNeckY;
        player_neck.w = playerNeckW;
        player_neck.h = playerNeckH;
        player_torso.x = playerTorsoX;
        player_torso.y = playerTorsoY;
        player_torso.w = playerTorsoW;
        player_torso.h = playerTorsoH;
        player_right_leg.x = playerRightLegX;
        player_right_leg.y = playerRightLegY;
        player_right_leg.w = playerRightLegW;
        player_right_leg.h = playerRightLegH;
        player_right_foot.x = playerRightFootX;
        player_right_foot.y = playerRightFootY;
        player_right_foot.w = playerRightFootW;
        player_right_foot.h = playerRightFootH;
        // PHASE 5: get render color based on iteration
        switch (i) {
            case 0:
                SDL_SetRenderDrawColor(sdlRenderer, 255, 0, 0, 255);
                break;
            case 1:
                SDL_SetRenderDrawColor(sdlRenderer, 0, 255, 0, 255);
                break;
            case 2:
                SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 255, 255);
                break;
            default:
                // this will never be needed but g++ might yell
                break;
        }
        // PHASE 6: render all rectangles
        SDL_RenderFillRect(sdlRenderer, &player_antenna);
        SDL_RenderFillRect(sdlRenderer, &player_head);
        SDL_RenderFillRect(sdlRenderer, &player_eye);
        SDL_RenderFillRect(sdlRenderer, &player_neck);
        SDL_RenderFillRect(sdlRenderer, &player_torso);
        SDL_RenderFillRect(sdlRenderer, &player_right_leg);
        SDL_RenderFillRect(sdlRenderer, &player_right_foot);
    }
    //configure renderer
    SDL_SetRenderDrawBlendMode(sdlRenderer, SDL_BLENDMODE_NONE);
    SDL_SetRenderDrawColor(sdlRenderer, 255, 105, 180, 255);
    //make hitbox
    if (showHitbox) {
        // PHASE 1: calculate values
        double line1X1 = x;
        double line1Y1 = y;
        double line1X2 = x+width;
        double line1Y2 = y;
        double line2X1 = x;
        double line2Y1 = y+height;
        double line2X2 = x+width;
        double line2Y2 = y+height;
        double line3X1 = x;
        double line3Y1 = y;
        double line3X2 = x;
        double line3Y2 = y+height;
        double line4X1 = x+width;
        double line4Y1 = y;
        double line4X2 = x+width;
        double line4Y2 = y+height;
        // PHASE 2: translate world space to screen space
        line1X1 *= SCREEN_WIDTH;
        line1Y1 *= SCREEN_HEIGHT;
        line1X2 *= SCREEN_WIDTH;
        line1Y2 *= SCREEN_HEIGHT;
        line2X1 *= SCREEN_WIDTH;
        line2Y1 *= SCREEN_HEIGHT;
        line2X2 *= SCREEN_WIDTH;
        line2Y2 *= SCREEN_HEIGHT;
        line3X1 *= SCREEN_WIDTH;
        line3Y1 *= SCREEN_HEIGHT;
        line3X2 *= SCREEN_WIDTH;
        line3Y2 *= SCREEN_HEIGHT;
        line4X1 *= SCREEN_WIDTH;
        line4Y1 *= SCREEN_HEIGHT;
        line4X2 *= SCREEN_WIDTH;
        line4Y2 *= SCREEN_HEIGHT;
        // PHASE 3: render lines
        SDL_RenderDrawLine(sdlRenderer, line1X1, line1Y1, line1X2, line1Y2);
        SDL_RenderDrawLine(sdlRenderer, line2X1, line2Y1, line2X2, line2Y2);
        SDL_RenderDrawLine(sdlRenderer, line3X1, line3Y1, line3X2, line3Y2);
        SDL_RenderDrawLine(sdlRenderer, line4X1, line4Y1, line4X2, line4Y2);
    }
    //restore render color
    SDL_SetRenderDrawColor(sdlRenderer, 255, 255, 255, 255);
}

/**
 * This modifies the player's score
 * 
 * @param delta the number of points to add onto the player's score
 */
void Player::updateScore(int delta) {
    int newScore = score + delta;
    if (newScore<1000 && newScore>=0) {
        score = newScore;
    }
}

/**
 * This toggles the glitch effect for the player
 */
void Player::toggleGlitched() {
    isGlitched = !isGlitched;
}

/**
 * This renders player physics
 */
void Player::runPhysics() {
    // make the player's hitbox easier to access
    double playerLeft = x;
    double playerRight = x + width;
    double playerTop = y;
    double playerBottom = y + height;
    for (std::list<Block>::iterator i = blocks->begin(); i != blocks->end(); std::advance(i,1)) {
        // make the player's hitbox easier to access
        double blockLeft = i->x;
        double blockRight = i->x + i->width;
        double blockTop = i->y;
        double blockBottom = i->y + i->height;
        // compare directions
        if (playerRight < blockLeft) continue; // player is left of the block
        if (playerLeft > blockRight) continue; // player is right of the block
        if (playerBottom < blockTop) continue; // player is above the block
        if (playerTop > blockBottom) continue; // player is below the block
        // NOTE: at this point, the player is inside a block
        // reset jump count
        jumpCount = 0;
        // claim the block's point if possible
        if (i->claimScore()) {
            updateScore(1);
        }
        // pull player on top of the block
        y = blockTop - height;
        // return since the player has been processed
        return;
    }
}

/**
 * This toggles if the player's hitbox should be seen
 */
void Player::toggleHitbox() {
    showHitbox = !showHitbox;
}

/**
 * This updates the player's variables each frame
 * 
 * Things handled:
 *      - blocks pushing player
 *      - gravity
 *      - jumping
 *      - syncing block movement with player
 *      - checking for death
 * 
 * NOTE: The formula for jumping is very odd. It will most likely break if touched.
 */
void Player::update() {
    if (isAlive) {
        // check for intersecting blocks
        runPhysics();
        // apply gravity and jump
        // NOTE: the player exists in world space so do not translate to screen space
        double jumpCalculation = JUMP_BOOST * sin(jumpTime * JUMP_TIME_MAX * 3.14) * CLOCK_DELTA;
        double gravityCalculation = GRAVITY * CLOCK_DELTA;
        transformY(gravityCalculation - jumpCalculation);
        // process jumping
        if (jumpTime > 0) {
            // update jump timer
            jumpTime -= CLOCK_DELTA;
        } else if (jumpTime < 0) {
            jumpTime = 0;
        }
        // check for player not jumping
        if (jumpTime <= 0) {
            // sync block movement with player
            transformX(-1*BLOCK_SPEED*CLOCK_DELTA);
        }
        // check for a death
        checkDeath();
    } else {
        // pull player down, even in death
        transformY(GRAVITY * CLOCK_DELTA);
    }
}

/**
 * This processes a request for the player to jump
 */
void Player::jump() {
    if (isAlive && jumpCount < jumpLimit) {
        jumpTime = JUMP_TIME_MAX;
        jumpCount++;
    }
}