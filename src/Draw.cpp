// import needed file
#include "Draw.h"

/**
 * This renders the death screen message
 * 
 * @param rend the SDL_Renderer to use for drawing
 */
void deathScreen(SDL_Renderer *rend) {
    //update renderer
    SDL_SetRenderDrawColor(rend, 255, 0, 0, 255);
    SDL_RenderClear(rend);
    SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
    float center = SCREEN_WIDTH / 2;
    float stepX = SCREEN_WIDTH / 16;
    float stepY = SCREEN_HEIGHT / 9;
    //Y
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY     , center-(stepX*4) , stepY*2   );
    SDL_RenderDrawLine(rend, center-(stepX*3) , stepY     , center-(stepX*4) , stepY*2   );
    SDL_RenderDrawLine(rend, center-(stepX*4) , stepY*2   , center-(stepX*4) , stepY*4   );
    //O
    SDL_RenderDrawLine(rend, center           , stepY     , center-stepX     , stepY*2   );
    SDL_RenderDrawLine(rend, center           , stepY     , center+stepX     , stepY*2   );
    SDL_RenderDrawLine(rend, center-stepX     , stepY*2   , center-stepX     , stepY*3   );
    SDL_RenderDrawLine(rend, center+stepX     , stepY*2   , center+stepX     , stepY*3   );
    SDL_RenderDrawLine(rend, center           , stepY*4   , center-stepX     , stepY*3   );
    SDL_RenderDrawLine(rend, center           , stepY*4   , center+stepX     , stepY*3   );
    //U
    SDL_RenderDrawLine(rend, center+(stepX*3) , stepY     , center+(stepX*3) , stepY*3   );
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY     , center+(stepX*5) , stepY*3   );
    SDL_RenderDrawLine(rend, center+(stepX*3) , stepY*3   , center+(stepX*4) , stepY*4   );
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY*3   , center+(stepX*4) , stepY*4   );
    //D
    SDL_RenderDrawLine(rend, center-(stepX*7) , stepY*5   , center-(stepX*7) , stepY*8   );
    SDL_RenderDrawLine(rend, center-(stepX*7) , stepY*5   , center-(stepX*6) , stepY*5   );
    SDL_RenderDrawLine(rend, center-(stepX*7) , stepY*8   , center-(stepX*6) , stepY*8   );
    SDL_RenderDrawLine(rend, center-(stepX*6) , stepY*5   , center-(stepX*5) , stepY*6   );
    SDL_RenderDrawLine(rend, center-(stepX*6) , stepY*8   , center-(stepX*5) , stepY*7   );
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY*6   , center-(stepX*5) , stepY*7   );
    //I
    SDL_RenderDrawLine(rend, center-stepX     , stepY*5   , center-(stepX*3) , stepY*5   );
    SDL_RenderDrawLine(rend, center-(stepX*2) , stepY*5   , center-(stepX*2) , stepY*8   );
    SDL_RenderDrawLine(rend, center-stepX     , stepY*8   , center-(stepX*3) , stepY*8   );
    //E
    SDL_RenderDrawLine(rend, center+stepX     , stepY*5   , center+stepX     , stepY*8   );
    SDL_RenderDrawLine(rend, center+stepX     , stepY*5   , center+(stepX*3) , stepY*5   );
    SDL_RenderDrawLine(rend, center+stepX     , stepY*8   , center+(stepX*3) , stepY*8   );
    SDL_RenderDrawLine(rend, center+stepX     , stepY*6.5 , center+(stepX*3) , stepY*6.5 );
    //D
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY*5   , center+(stepX*5) , stepY*8   );
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY*5   , center+(stepX*6) , stepY*5   );
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY*8   , center+(stepX*6) , stepY*8   );
    SDL_RenderDrawLine(rend, center+(stepX*6) , stepY*5   , center+(stepX*7) , stepY*6   );
    SDL_RenderDrawLine(rend, center+(stepX*6) , stepY*8   , center+(stepX*7) , stepY*7   );
    SDL_RenderDrawLine(rend, center+(stepX*7) , stepY*6   , center+(stepX*7) , stepY*7   );
    //draw lines
    SDL_RenderPresent(rend);
}

/**
 * Draws a number from 0-9 on the screen
 * 
 * @param rend the SDL_Renderer to render the number with
 * @param number an integer from 0-9
 * @param startX the top left X coordinate
 * @param stopX the bottom right X coordinate
 * @param startY the top left Y coordinate
 * @param stopY the bottom right Y coordinate
 */
void drawNumber(SDL_Renderer *rend, int number, int startX, int stopX, int startY, int stopY) {
    // ensure draw color is white
    SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
    // make middle y for more some numbers
    int middleY = startY+stopY/2;
    switch (number) {
        case 0:
            //    ###
            //    # #
            //    # #
            //    # #
            //    ###
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw left line
            SDL_RenderDrawLine(rend, startX, startY, startX, stopY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, stopY);
            // draw bottom line
            SDL_RenderDrawLine(rend, startX, stopY, stopX, stopY);
            // exit switch
            break;
        case 1:
            //      #
            //      #
            //      #
            //      #
            //      #
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, stopY);
            // exit switch
            break;
        case 2:
            //    ###
            //      #
            //    ###
            //    #
            //    ###
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw left line
            SDL_RenderDrawLine(rend, startX, middleY, startX, stopY);
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, middleY);
            // draw bottom line
            SDL_RenderDrawLine(rend, startX, stopY, stopX, stopY);
            // exit switch
            break;
        case 3:
            //    ###
            //      #
            //    ###
            //      #
            //    ###
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, stopY);
            // draw bottom line
            SDL_RenderDrawLine(rend, startX, stopY, stopX, stopY);
            // exit switch
            break;
        case 4:
            //    # #
            //    # #
            //    ###
            //      #
            //      #
            // draw left line
            SDL_RenderDrawLine(rend, startX, startY, startX, middleY);
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, stopY);
            // exit switch
            break;
        case 5:
            //    ###
            //    #
            //    ###
            //      #
            //    ###
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw left line
            SDL_RenderDrawLine(rend, startX, startY, startX, middleY);
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, middleY, stopX, stopY);
            // draw bottom line
            SDL_RenderDrawLine(rend, startX, stopY, stopX, stopY);
            // exit switch
            break;
        case 6:
            //    ###
            //    #
            //    ###
            //    # #
            //    ###
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw left line
            SDL_RenderDrawLine(rend, startX, startY, startX, stopY);
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, middleY, stopX, stopY);
            // draw bottom line
            SDL_RenderDrawLine(rend, startX, stopY, stopX, stopY);
            // exit switch
            break;
        case 7:
            //    ###
            //      #
            //      #
            //      #
            //      #
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, stopY);
            // exit switch
            break;
        case 8:
            //    ###
            //    # #
            //    ###
            //    # #
            //    ###
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw left line
            SDL_RenderDrawLine(rend, startX, startY, startX, stopY);
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, stopY);
            // draw bottom line
            SDL_RenderDrawLine(rend, startX, stopY, stopX, stopY);
            // exit switch
            break;
        case 9:
            //    ###
            //    # #
            //    ###
            //      #
            //    ###
            // draw top line
            SDL_RenderDrawLine(rend, startX, startY, stopX, startY);
            // draw left line
            SDL_RenderDrawLine(rend, startX, startY, startX, middleY);
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            // draw right line
            SDL_RenderDrawLine(rend, stopX, startY, stopX, stopY);
            // draw bottom line
            SDL_RenderDrawLine(rend, startX, stopY, stopX, stopY);
            // exit switch
            break;
        default:
            //
            //
            //    ###
            //
            //
            // draw middle line
            SDL_RenderDrawLine(rend, startX, middleY, stopX, middleY);
            break;
    }
}

/**
 * This renders a score to the screen
 * 
 * @param rend the SDL_Renderer for drawing graphics
 * @param score the score expressed as an int
 */
void drawScore(SDL_Renderer *rend, int score) {
    int center = SCREEN_WIDTH / 2;
    int width = 40;
    int gap = 10;
    int yShift = 0;
    int height = 80;
    int centerLeft = center-width/2;
    int centerRight = center+width/2;
    // bounds for X--
    if (score >= 100) {
        drawNumber(rend, (score%1000-score%100)/100, centerLeft-gap-width, centerLeft-gap, yShift, height);
    } else if (score < 0) {
        drawNumber(rend, -1, centerLeft-gap-width, centerLeft-gap, yShift, height);
    } else {
        drawNumber(rend, 0, centerLeft-gap-width, centerLeft-gap, yShift, height);
    }
    // bounds for -X-
    if (score >= 10) {
        drawNumber(rend, (score%100-score%10)/10, centerLeft, centerRight, yShift, height);
    } else if (score < 0) {
            drawNumber(rend, -1, centerLeft, centerRight, yShift, height);
    } else {
        drawNumber(rend, 0, centerLeft, centerRight, yShift, height);
    }
    // bounds for --X
    if (score >= 0) {
        drawNumber(rend, score%10, centerRight+gap, centerRight+gap+width, yShift, height);
    } else {
        drawNumber(rend, -1, centerRight+gap, centerRight+gap+width, yShift, height);
    }
}

/**
 * This renders a paused message for game state 0
 * 
 * @param rend the SDL_Renderer used for drawing graphics
 */
void pauseScreen(SDL_Renderer *rend) {
    //update renderer
    SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
    SDL_RenderClear(rend);
    SDL_SetRenderDrawColor(rend, 255, 255, 255, 255);
    float center = SCREEN_WIDTH / 2;
    float stepX = SCREEN_WIDTH / 23;
    float stepY = SCREEN_HEIGHT / 9;
    //P
    SDL_RenderDrawLine(rend, center-(stepX*9) , stepY     , center-(stepX*8) , stepY      );
    SDL_RenderDrawLine(rend, center-(stepX*9) , stepY*2.5 , center-(stepX*8) , stepY*2.5  );
    SDL_RenderDrawLine(rend, center-(stepX*9) , stepY     , center-(stepX*9) , stepY*4    );
    SDL_RenderDrawLine(rend, center-(stepX*8) , stepY     , center-(stepX*7) , stepY*1.75 );
    SDL_RenderDrawLine(rend, center-(stepX*8) , stepY*2.5 , center-(stepX*7) , stepY*1.75 );
    //R
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY     , center-(stepX*4) , stepY      );
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY*2.5 , center-(stepX*4) , stepY*2.5  );
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY     , center-(stepX*5) , stepY*4    );
    SDL_RenderDrawLine(rend, center-(stepX*4) , stepY     , center-(stepX*3) , stepY*1.75 );
    SDL_RenderDrawLine(rend, center-(stepX*4) , stepY*2.5 , center-(stepX*3) , stepY*1.75 );
    SDL_RenderDrawLine(rend, center-(stepX*4) , stepY*2.5 , center-(stepX*3) , stepY*4    );
    //E
    SDL_RenderDrawLine(rend, center-(stepX*1) , stepY     , center+(stepX*1) , stepY      );
    SDL_RenderDrawLine(rend, center-(stepX*1) , stepY*2.5 , center+(stepX*1) , stepY*2.5  );
    SDL_RenderDrawLine(rend, center-(stepX*1) , stepY*4   , center+(stepX*1) , stepY*4    );
    SDL_RenderDrawLine(rend, center-(stepX*1) , stepY     , center-(stepX*1) , stepY*4    );
    //S
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY*2   , center+(stepX*4) , stepY      );
    SDL_RenderDrawLine(rend, center+(stepX*4) , stepY     , center+(stepX*3) , stepY*2    );
    SDL_RenderDrawLine(rend, center+(stepX*3) , stepY*2   , center+(stepX*5) , stepY*3    );
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY*3   , center+(stepX*4) , stepY*4    );
    SDL_RenderDrawLine(rend, center+(stepX*4) , stepY*4   , center+(stepX*3) , stepY*3    );
    //S
    SDL_RenderDrawLine(rend, center+(stepX*9) , stepY*2   , center+(stepX*8) , stepY      );
    SDL_RenderDrawLine(rend, center+(stepX*8) , stepY     , center+(stepX*7) , stepY*2    );
    SDL_RenderDrawLine(rend, center+(stepX*7) , stepY*2   , center+(stepX*9) , stepY*3    );
    SDL_RenderDrawLine(rend, center+(stepX*9) , stepY*3   , center+(stepX*8) , stepY*4    );
    SDL_RenderDrawLine(rend, center+(stepX*8) , stepY*4   , center+(stepX*7) , stepY*3    );
    
    //S
    SDL_RenderDrawLine(rend, center-(stepX*7) , stepY*6   , center-(stepX*8) , stepY*5    );
    SDL_RenderDrawLine(rend, center-(stepX*8) , stepY*5   , center-(stepX*9) , stepY*6    );
    SDL_RenderDrawLine(rend, center-(stepX*9) , stepY*6   , center-(stepX*7) , stepY*7    );
    SDL_RenderDrawLine(rend, center-(stepX*7) , stepY*7   , center-(stepX*8) , stepY*8    );
    SDL_RenderDrawLine(rend, center-(stepX*8) , stepY*8   , center-(stepX*9) , stepY*7    );
    //P
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY*5   , center-(stepX*4) , stepY*5    );
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY*6.5 , center-(stepX*4) , stepY*6.5  );
    SDL_RenderDrawLine(rend, center-(stepX*4) , stepY*5   , center-(stepX*3) , stepY*5.75 );
    SDL_RenderDrawLine(rend, center-(stepX*4) , stepY*6.5 , center-(stepX*3) , stepY*5.75 );
    SDL_RenderDrawLine(rend, center-(stepX*5) , stepY*5   , center-(stepX*5) , stepY*8    );
    //A
    SDL_RenderDrawLine(rend, center-(stepX*1) , stepY*5   , center+(stepX*1) , stepY*5    );
    SDL_RenderDrawLine(rend, center-(stepX*1) , stepY*6.5 , center+(stepX*1) , stepY*6.5  );
    SDL_RenderDrawLine(rend, center-(stepX*1) , stepY*5   , center-(stepX*1) , stepY*8    );
    SDL_RenderDrawLine(rend, center+(stepX*1) , stepY*5   , center+(stepX*1) , stepY*8    );
    //C
    SDL_RenderDrawLine(rend, center+(stepX*5) , stepY*5   , center+(stepX*4) , stepY*5    );
    SDL_RenderDrawLine(rend, center+(stepX*4) , stepY*5   , center+(stepX*3) , stepY*6    );
    SDL_RenderDrawLine(rend, center+(stepX*3) , stepY*6   , center+(stepX*3) , stepY*7    );
    SDL_RenderDrawLine(rend, center+(stepX*3) , stepY*7   , center+(stepX*4) , stepY*8    );
    SDL_RenderDrawLine(rend, center+(stepX*4) , stepY*8   , center+(stepX*5) , stepY*8    );
    //E
    SDL_RenderDrawLine(rend, center+(stepX*7) , stepY*5   , center+(stepX*9) , stepY*5    );
    SDL_RenderDrawLine(rend, center+(stepX*7) , stepY*6.5 , center+(stepX*9) , stepY*6.5  );
    SDL_RenderDrawLine(rend, center+(stepX*7) , stepY*8   , center+(stepX*9) , stepY*8    );
    SDL_RenderDrawLine(rend, center+(stepX*7) , stepY*5   , center+(stepX*7) , stepY*8    );
    //draw lines
    SDL_RenderPresent(rend);
}

/**
 * This renders all graphics for game state 1
 * 
 * @param blocks the list of blocks to be rendered
 * @param rend the SDL_Renderer to be used for drawing graphics
 * @param player the player to be rendered
 */
void renderGameScene(std::list <Block> blocks, SDL_Renderer *rend, Player *player) {
    SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
    SDL_RenderClear(rend);
    for (std::list<Block>::iterator i = blocks.begin(); i != blocks.end(); std::advance(i,1)) {
        i->render();
    }
    player->render();
    drawScore(rend, player->score);
    SDL_RenderPresent(rend);
}

void renderYieldScene(std::list <Block> blocks, SDL_Renderer *rend, Player *player) {
    // render game scene
    SDL_SetRenderDrawColor(rend, 0, 0, 0, 255);
    SDL_RenderClear(rend);
    for (std::list<Block>::iterator i = blocks.begin(); i != blocks.end(); std::advance(i,1)) {
        i->render();
    }
    player->render();
    drawScore(rend, player->score);
    // render overlay
    SDL_SetRenderDrawColor(rend, 255, 0 , 0, 75);
    SDL_SetRenderDrawBlendMode(rend, SDL_BLENDMODE_BLEND);
    SDL_Rect fillMe;
    fillMe.x = 0;
    fillMe.y = 0;
    fillMe.w = SCREEN_WIDTH;
    fillMe.h = SCREEN_HEIGHT;
    SDL_RenderFillRect(rend, &fillMe);
    SDL_SetRenderDrawBlendMode(rend, SDL_BLENDMODE_NONE);
    SDL_RenderPresent(rend);
}