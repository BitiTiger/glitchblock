#!/bin/bash

# ensure SDL2 is installed
sdl2-config --version > /dev/null 2> /dev/null
if [ $? != 0 ]; then
    # check if Arch
    grep "ID_LIKE=arch" /etc/os-release 2> /dev/null
    if [ $? == 0 ]; then
        echo "Installing SDL2 libraries for Arch Linux..."
        sudo pacman -S sdl2
    else
        # check if Debian
        grep "ID_LIKE=debian" /etc/os-release 2> /dev/null
        if [ $? == 0 ]; then
            echo "Installing SDL2 libraries for Debian Linux..."
            sudo apt install libsdl2-2.0-0
        else
            # display error
            echo "You must install SDL2 libraries for your Linux distribution before playing this game."
            exit
        fi
    fi
fi

# run game
exec "./glitchblock"