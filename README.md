# Glitchblock

This is a proof of concept game designed in SDL2/C++ for Linux.
This demo does not work natively with Windows 10, but will work with WSL if configured correctly.

![screenshot](Screenshot.png)

## Compile

**Note**: Compile instructions may vary depending on your Linux distro.

### To compile:

1. `git clone https://gitlab.com/caton101/glitchblock`
2. `cd glitchblock/src`
3. `chmod +x run`
4. `./run`

### Required libraries:

- Ubuntu: `sudo apt install gcc libsdl2-dev`
- Arch/Manjaro: `sudo pacman -S gcc sdl2 `

## Usage

`./run`

NOTE: The run script may recompile the code if the source files were modified. See the section on compiling for more info.

## Key Bindings

### Waiting to start:
| Key   | Purpose                   |
| :-:   | :------------------------ |
| esc   | exit game                 |
| q     | exit game                 |
| space | start game                |

### During gameplay:
| Key   | Purpose                   |
| :-:   | :------------------------ |
| w     | move up                   |
| a     | move left                 |
| s     | move down                 |
| d     | move right                |
| space | jump                      |
| esc   | exit game                 |
| `     | activate debug keys       |

### During gameplay (with debug):
| Key   | Purpose                       |
| :-:   | :---------------------------- |
| w     | move player up                |
| a     | move player left              |
| s     | move player down              |
| d     | move player right             |
| space | jump                          |
| esc   | exit game                     |
| `     | deactivate debug keys         |
| q     | exit game                     |
| e     | toggle hitbox (player)        |
| r     | toggle glitch effect (player) |
| z     | show debug info (player)      |
| i     | move all blocks up            |
| j     | move all blocks left          |
| k     | move all blocks down          |
| l     | move all blocks right         |
| o     | toggle hitbox (blocks)        |
| p     | toggle glitch effect (blocks) |
| m     | show debug info (blocks)      |

### Death Screen:
| Key   | Purpose                   |
| :-:   | :------------------------ |
| esc   | exit game                 |
| q     | exit game                 |
| space | exit game                 |